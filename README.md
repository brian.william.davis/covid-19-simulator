COVID-19 Simulator
==================

![population health](static/health_example.gif)
![population immunity](static/immunity_example.gif)
![pathogen spread](static/pathogen_example.gif)

This is just a little experiment to see if I can simulate the spread
of COVID-19 and other viruses within a community, using only a few
variables.

__NOTE: I am not a virologist nor an epidemiologist. Please do not in
any way construe anything I say, do, write or share here to be
official, reputable, reliable, or correct. This is a toy, and is not
intended to model or predict real-world epidemics. Those tools are
much more nuanced and accurate, and have the added benefit of being
designed and used by certified experts. Of which, again, I am not
one.__

This simulation is a matrix-based model and consists of a fixed
population of hosts. Each host starts the simulation with an age and
health rating. The combination of age and health determines a host's
ability to resist pathogen load and, when infected, fight virulent
pathogens.

This simulation proceeds in daily increments and uses stochastic
methods to create, infect, kill, and heal hosts. For the purposes of
simplicity, there are no births, no urban design, and no
non-pathogen-related deaths. At the end of each day, the simulation
will print out a summary of the pathogen's spread through the
population.

Prior to starting the simulation, the user will need to initialize a
few variables (pathogen factors, population size, demographics, number
of initial patients, etc). Once initialized, the simulator will create
a population based off of the demographics provided -- with a
realistic distribution of ages and relative health -- inoculate one or
more of them with the pathogen, and begin the simulation.

This simulation places hosts on a two-dimensional grid. Infected hosts
shed pathogens onto their neighbors on grid, dispersing the pathogen
in amounts and at a distance defined by their level of infection,
mobility, social contact, etc. Mobility across the grid is a function
of age, peaking in middle age and dropping at either end of the age
range. (Note: Mobility is not yet implemented.)

Hosts (infected or not) pick up some percentage of these pathogens
that fall upon their grid cell. We'll use weighted-random sampling to
simulate area of movement and host-to-host interactions.


Usage
-----

To run the simulator, simply navigate to an empty directory and invoke
it thusly:

`$ ../simulator.py --pathogen ../covid-19.json --population ../test_population.json --days 100 --sources 5`

This will dump 100 images into the directory -- one for each day of
the simulation with the red channel dedicated to pathogen
concentration and the green channel devoted to host immunity. I've
included a handy video generation script to compile these images into
little MP4-formatted movies.

To make a movie from your images:

`$ ../make_movies.sh`

You will need to have [ffmpeg](https://www.ffmpeg.org/) installed in
order for this script to work.


Configuration
-------------

To initialize the simulation, you'll need to provide it with two
JSON-formatted configuration files -- one each for the pathogen and
the host population.


### Pathogen configuration

The pathogen JSON file should contain, at minimum, the following keys:

  * `virulence` -- a floating point number between 0.0 and 1.0
    representing the case fatality rate (CFR) of the disease
  * `transmissibility` -- a floating point number greater than zero
    representing the pathogen's reproduction number (R0)
  * `incubation` -- a floating point number representing the average
    number of days that a host may carry the pathogen without
    transmitting it in any significant quantity to others
  * `contagious` -- a floating point number representing the the
    average number of days that a host is shedding significant
    quantities of the pathogen
  * `persistence` -- a floating point number representing the
    half-life of viable spores in the environment
  * `susceptible` -- an array of floats between 0.0 and 1.0
    representing the percentage of each cohort that is particularly
    susceptible to this pathogen (age-group-specific CFR)


### Host population configuration

The host population file should contain, at a minimum, the following
keys:

  * `population` -- an integer representing the total population size
  * `contact` -- an integer representing the average number of people
    each host interacts with on a daily basis
  * `mobility` -- a float representing the percentage of the
    population that is traveling on any given day
  * `beds` -- a float representing the number of hospital beds per
    1000 people available to the population
  * `demographics` -- an array of floats between 0.0 and 1.0
    representing each cohort's percentage of the total population,
    where the cohorts are 0-9 years of age, 10-19, 20-29, 30-39,
    40-49, 50-59, 60-69, 70-79, 80-89, 90+
  * `measures` -- an array of dicts of the format `{"day": <day>,
    <parameter>: <value>}` representing public health measures that
    alter population norms, where `day` is the number of days after
    the pandemic starts (simulation day) and `<parameter>` may
    currently be one of `"contact"` or `"mobility"`. If one or the
    other is not specified, its previous value will continue to be
    used.
