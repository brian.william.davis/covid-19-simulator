#!/usr/bin/env python3

import numpy as np
import scipy as sp
import scipy.ndimage
from PIL import Image


# pathogens need:
# - a virulence factor -- the harm they inflict on a host (a proxy to reproduction rate in the host?)
# - a transmission factor -- the ease with which they infect new hosts
# - an incubation period -- the time during which they are not contagious
# - a contagious period -- the time during which they are communicable
# - a persistence period -- the time during which spores remain viable outside a host
class Pathogen:
    def __init__(self, pathogen_config):
        self.virulence = pathogen_config['virulence']
        self.transmissibility = pathogen_config['transmissibility']
        self.incubation = pathogen_config['incubation']
        self.contagious = pathogen_config['contagious']
        self.persistence = pathogen_config['persistence']
        self.susceptible = np.array(pathogen_config['susceptible'])

        # Calculate some derived properties based on these values.
        self.daily_growth_rate = np.exp(1.0 / self.incubation)


class Population:
    def __init__(self, population_config):
        # Convert the provided population into the nearest even square
        # number, to make constructing a square grid (and movies)
        # easier.
        self.size = (2 * int(np.sqrt(population_config['population']) / 2.0)) ** 2
        self.contact = population_config.get('contact', 10)
        self.mobility = population_config.get('mobility', 0.01)
        self.beds = int(population_config.get('beds', 4.0) * self.size / 1000)
        self.demographics = np.array(population_config.get('demographics', [0.1]*10))

        # Sort measures in reverse chronological order.
        self.measures = sorted(population_config.get('measures', []),
                               key=lambda x: x['day'],
                               reverse=True)


# NOTES:
# - contact between age groups is not uniform (https://www.researchgate.net/figure/The-total-number-of-contacts-between-age-groups-in-EpiSimS-The-contact-rates-are-defined_fig4_228649013)
class Simulation:
    def __init__(self, pathogen_config, population_config, patient_zeros, generate_images):
        self.generate_images = generate_images
        self.day = 0
        self.deaths = 0
        self.critical = 0
        self.contagious = 0
        self.pathogen = Pathogen(pathogen_config)
        self.population = Population(population_config)
        self.health_care_system_stress = 0.0
        self.critical_threshold = 0.25
        self.contagious_threshold = 0.005

        # Initialize the grids.
        side = int(np.sqrt(self.population.size))
        self.pathogen_grid = np.zeros((side, side))
        self.immunity_grid = np.zeros((side, side))

        # Generate a noise grid to smooth out the discrete values in
        # our age and health grids.
        noise_grid = np.random.normal(0, 0.1, (side, side))

        # FIXME: The real way to do this is to fit a distribution
        # function to the demographics data, then randomly sample from
        # that curve.
        #dist = scipy.stats.rv_histogram((self.population.demographics,
        #                                 np.arange(0, 110, 10)))
        #self.age_grid = dist.rvs(side * side).reshape((side, side))
        ages = np.arange(0, 100, 10)
        self.age_grid = np.random.choice(ages, size=(side, side), p=self.population.demographics)

        health_grid = self.age_grid.astype(np.float32)
        for x in range(ages.size):
            health_grid = np.where(self.age_grid == ages[x],
                                   1 - self.pathogen.susceptible[x],
                                   health_grid)
        self.health_grid = health_grid + noise_grid

        # Keep health_grid in the [0.0, 1.0) range.
        self.health_grid = self.health_grid / self.health_grid.max()
        self.baseline_health_grid = np.copy(self.health_grid)

        # Inoculate our patient zero(s) with a massive dose of pathogen.
        for x in range(patient_zeros):
            pz = (np.random.randint(0, side), np.random.randint(0, side))
            self.pathogen_grid[pz] = 0.01

    def _swap_pixels(self, grid, a_coords, b_coords):
        x_a, y_a = a_coords
        x_b, y_b = b_coords
        temp = grid[x_a, y_a]
        grid[x_a, y_a] = grid[x_b, y_b]
        grid[x_b, y_b] = temp

    def implement_measures(self):
        """Adjust social contact in response to public health measures."""

        # Check if there are new measures today.
        if len(self.population.measures) > 0 and self.day >= self.population.measures[-1]['day']:
            # If so, update social contact and get rid of the measure
            # record so we can look for the next one.
            measure = self.population.measures.pop()
            self.population.contact = measure.get('contact', self.population.contact)
            self.population.mobility = measure.get('mobility', self.population.mobility)

    def mix_hosts(self):
        # Mix hosts randomly based on the population mobility
        # settings.

        # Pick a fixed number of pixels at random (we divide mobility
        # by half because each pixel in num is swapping with a
        # corresponding partner pixel).
        num = int(self.population.size * self.population.mobility * 0.5)
        side = int(np.sqrt(self.population.size))
        pixels = np.random.randint(0, side, size=(num, 2))

        # For each pixel, pick a partner pixel to swap with. The
        # distance between these partner pixels is
        # log-series-weighted, so there's an exponentially stronger
        # preference for closer pixels. However, the shape of the
        # distribution is also influenced by the mobility
        # configuration setting, with larger mobility settings
        # resulting in greater average travel distance.

        # FIXME: Magic numbers alert! I don't like this treatment of
        # mobility.
        distance = np.random.logseries(np.power(self.population.mobility, 0.1), size=num)
        angle = np.random.randint(0, 360, size=num) * (np.pi / 180.0)

        # Convert polar coordinates to cartesian.
        partners = np.zeros((num, 2), dtype=np.int32)
        partners[:,0] = (distance * np.cos(angle)).astype(np.int32) % side
        partners[:,1] = (distance * np.sin(angle)).astype(np.int32) % side

        grids = [self.age_grid,
                 self.health_grid,
                 self.baseline_health_grid,
                 self.pathogen_grid,
                 self.immunity_grid]

        for i in range(num):
            health_a = self.baseline_health_grid[pixels[i][0], pixels[i][1]]
            health_b = self.baseline_health_grid[partners[i][0], partners[i][1]]

            # Healthy folks only. Critically ill people generally
            # don't travel or mix.
            if health_a > self.critical_threshold and health_b > self.critical_threshold:
                for grid in grids:
                    self._swap_pixels(grid, pixels[i], partners[i])

    def grow_pathogens(self):
        # Increase the pathogen population by its growth rate.
        #
        # Pathogen growth is a function of host immunity and existing
        # pathogen concentration. Absent host immunity, pathogens
        # reproduce at an exponential rate. However, this rate is
        # retarded by host immunity. The higher the immunity, the
        # lower the pathogen growth rate. With a high enough immunity,
        # the pathogen growth rate begins to fall.
        #
        # If the host is dead (i.e. health=0.0), then pathogens cannot
        # replicate on that cell and are wiped out.
        self.pathogen_grid = self.pathogen_grid * self.pathogen.daily_growth_rate * (1 - self.immunity_grid)
        self.pathogen_grid = np.where(self.health_grid == 0.0, 0.0, self.pathogen_grid)

    def spread_pathogens(self):
        # Calculate the base odds of one infected person transmitting
        # to a single other person today in a naive society practicing
        # zero mitigation measures.
        transmission_odds = self.pathogen.transmissibility / self.pathogen.contagious

        # Adjust for current social distancing measures (where 10.0 is
        # the reference contact metric for the R0 number in a naive
        # population).
        transmission_odds *= (self.population.contact / 10.0)

        # Disperse pathogens. The further from an infectious person,
        # the lower the likelihood of transmission.
        spread_factor = 1.0 # FIXME: magic number; should be a function of contact
        pathogens_shed = scipy.ndimage.filters.gaussian_filter(self.pathogen_grid,
                                                               spread_factor,
                                                               mode='wrap')

        spreaders = np.where(self.pathogen_grid >= self.contagious_threshold, 2.5, 0.0)

        # Calculate the odds of each person contracting the virus from
        # an infected neighbor, assuming one neighbor is infected each
        # day.
        exposed = scipy.ndimage.filters.gaussian_filter(spreaders,
                                                        spread_factor,
                                                        mode='wrap')

        # Because the Gaussian filter only spreads X% around and
        # leaves 1-X with the original spreader, we need to adjust the
        # odds by a factor.
        exposed = np.where(spreaders >= 1.0, 0, exposed)
        exposed = exposed / exposed.max()

        # Calculate the odds of any given person contracting the
        # pathogen. This should mean that close contacts of infected
        # persons have a much higher likelihood of contracting the
        # pathogen.
        exposed = exposed * transmission_odds

        # Flip a (weighted) coin to infect others. Use their odds to
        # weight the coin.
        #
        # NOTE: Applying this function is suuuuuuper slow. :(
        #infect = np.vectorize(lambda x: np.random.choice([0, 1],
        #                                                 p=[1.0 - x, x],
        #                                                 replace=True))
        #infected = infect(exposed)

        # NOTE: The loop above is so lovely, but so slow. I wonder if
        # we can mimic it more efficiently.
        #
        # 1. we generate a grid of random numbers
        #
        # 2. we multiply this grid with the exposed grid and then use
        # np.where to filter out values below a certain threshold
        # (dictated by social distancing regs?)
        #
        # 3. everything above that threshold is converted to a 1,
        # everything below becomes 0. this should have a strong bias
        # toward proximity to the spreaders.
        infected = np.where((np.random.random(size=exposed.shape) * exposed) > 0.05,
                            1,
                            0)


        # Finally, allocate the pathogens shed to each infected person
        # and zero out the pathogens_shed grid for any neighbors not
        # exposed.
        self.pathogen_grid += (pathogens_shed * infected)

        # Keep all values within the range [0.0, 1.0].
        self.pathogen_grid = np.where(self.pathogen_grid < 0.0, 0.0, self.pathogen_grid)
        self.pathogen_grid = np.where(self.pathogen_grid > 1.0, 1.0, self.pathogen_grid)
        self.pathogen_grid = np.where(self.health_grid == 0.0, 0.0, self.pathogen_grid)

    def adjust_host_immunity(self):

        """If pathogens are present, activate the immune response.

Only adjust the host's immunity if pathogens are present. The rate at
which the immune system responds is also a function of the host's
overall health.

        """
        self.immunity_grid += self.pathogen_grid * np.power(self.health_grid, 5) * 5

    def adjust_host_health(self):
        """If pathogens are present, adjust the host's health rating downward.

For the sake of simplicity, we choose to only use pathogen presence as
the driver of host health. If no pathogens are present, then the host
remains at their nominal ("baseline") health rating. In the presence
of infection, their health decreases proportional to the severity of
their infection.

We also use the availability of hospital beds to drive health. As the
ratio of seriously ill patients (health < 0.25) to beds grows,
outcomes are worse and community health declines across the board.

        """
        critical = (self.health_grid < self.critical_threshold) & (self.health_grid > 0.0)
        self.critical = np.count_nonzero(critical)
        self.health_care_system_stress = self.critical / self.population.beds
        weight = np.ones(self.pathogen_grid.shape) + (critical * self.health_care_system_stress)
        self.health_grid = self.baseline_health_grid - (self.pathogen_grid * weight * self.baseline_health_grid)

        # If health falls below zero, mark the host as deceased by
        # converting their baseline_health and immunity cell to 0.0.
        self.baseline_health_grid = np.where(self.health_grid <= 0.0, 0.0, self.baseline_health_grid)
        self.immunity_grid = np.where(self.health_grid <= 0.0, 0.0, self.immunity_grid)

    def save_day_image(self):
        # Save an image of the population to disk in a tagged
        # file. Use each layer as a different color channel (red =
        # pathogen, green = immunity, blue = health) to make a
        # composite image.
        img = np.zeros([self.health_grid.shape[0], self.health_grid.shape[1], 3], dtype=np.uint8)
        img[:,:,0] = (self.pathogen_grid * 255 * 3).astype(np.uint8)
        img[:,:,1] = (self.immunity_grid * 255).astype(np.uint8)

        # Because health clutters things, let's just use blue to
        # indicate survivors.
        #img[:,:,2] = ((self.health_grid > 0.0) * 128).astype(np.uint8)
        img[:,:,2] = (self.health_grid * 128).astype(np.uint8)
        i = Image.fromarray(img)
        i.save("outbreak_{:04d}.png".format(self.day))

    def generate_daily_stats(self):
        survivors = np.count_nonzero(self.baseline_health_grid)
        self.deaths = self.population.size - survivors
        self.contagious = np.count_nonzero(self.pathogen_grid > self.contagious_threshold)
        print("{},{},{},{},{:0.2f}".format(self.day,
                                           self.deaths,
                                           self.critical,
                                           self.contagious,
                                           self.health_care_system_stress))

    def run_one_day(self):
        self.implement_measures()
        self.mix_hosts()
        self.grow_pathogens()
        self.spread_pathogens()
        self.adjust_host_immunity()
        self.adjust_host_health()
        self.generate_daily_stats()

        if self.generate_images:
            self.save_day_image()

        self.day += 1

    def generate_run_stats(self):
        survivors = np.count_nonzero(self.baseline_health_grid)
        deaths = self.population.size - survivors
        deaths_pct = 100.0 * deaths / self.population.size
        demographics = np.where(self.baseline_health_grid <= 0.0, self.age_grid, np.nan)
        histogram = np.histogram(demographics, bins=10, range=(0, 100))

        def decade(x):
            return histogram[0][x]

        def dec_pct(x):
            return 100.0 * decade(x) / float(np.count_nonzero(np.where(self.age_grid == 10 * x,
                                                                       1,
                                                                       0)))

        print("=========================")
        print("days: {}".format(self.day))
        print("deaths: {} / {} ({:0.2f}%)".format(deaths, self.population.size, deaths_pct))
        print("   0-9: {} ({:0.2f}%)".format(decade(0), dec_pct(0)))
        print(" 10-19: {} ({:0.2f}%)".format(decade(1), dec_pct(1)))
        print(" 20-29: {} ({:0.2f}%)".format(decade(2), dec_pct(2)))
        print(" 30-39: {} ({:0.2f}%)".format(decade(3), dec_pct(3)))
        print(" 40-49: {} ({:0.2f}%)".format(decade(4), dec_pct(4)))
        print(" 50-59: {} ({:0.2f}%)".format(decade(5), dec_pct(5)))
        print(" 60-69: {} ({:0.2f}%)".format(decade(6), dec_pct(6)))
        print(" 70-79: {} ({:0.2f}%)".format(decade(7), dec_pct(7)))
        print(" 80-89: {} ({:0.2f}%)".format(decade(8), dec_pct(8)))
        print("   90+: {} ({:0.2f}%)".format(decade(9), dec_pct(9)))
        
    def run(self, days):
        # FIXME: if days == -1, run until there are no more pathogens
        # in the environment.
        print("day,deaths,critical,contagious,outbreak patients per bed")
        for x in range(days):
            self.run_one_day()

        # Dump some stats in text form!
        self.generate_run_stats()


if __name__ == '__main__':
    import click
    import json

    @click.command()
    @click.option('--days',
                  default=-1,
                  help='Number of days to run the simulation. To run until the endgame, use -1.')
    @click.option('--sources',
                  default=1,
                  help='Number of initial "patient zero" vectors in the community.')
    @click.option('--pathogen',
                  help='Path to pathogen JSON definition.')
    @click.option('--population',
                  help='Path to population JSON definition.')
    @click.option('--images/--no-images',
                  default=True,
                  help='Save population images for each day of the outbreak.')
    def run(pathogen, population, days, sources, images):
        with open(pathogen) as pathogen_file:
            pathogen_def = json.load(pathogen_file)
            with open(population) as population_file:
                population_def = json.load(population_file)

                sim = Simulation(pathogen_def, population_def, sources, images)
                sim.run(days)

    run()
