#!/bin/sh

ffmpeg -v 0 -framerate 5 -i outbreak_%04d.png -vf "drawtext=text='Day %{frame_num}':start_number=1:fontsize=20:x=(w-text_w)/2:y=h-(2*lh):box=1:boxcolor=white:boxborderw=5" -c:v libx264 -r 30 -pix_fmt yuv420p outbreak.mp4
